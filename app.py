from aiohttp import web
from route_table import add_routes

app = web.Application()
app.router.add_routes(add_routes())
#web.run_app(app)
