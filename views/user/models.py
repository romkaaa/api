import asyncio
import sqlalchemy as sa
from aiomysql.sa import create_engine

metadata = sa.MetaData()

tbl = sa.Table('tbl', metadata,
               sa.Column('id', sa.Integer, primary_key=True),
               sa.Column('val', sa.String(255)))

class User():
    async def add(self, loop):
        engine = await create_engine(user='root', db='cherry',
                                     host='127.0.0.1', password='onsapoengo', loop=loop)
        async with engine.acquire() as conn:
            await conn.execute(tbl.insert().values(val='abc'))
            await conn.execute(tbl.insert().values(val='xyz'))

            async for row in conn.execute(tbl.select()):
                print(row.id, row.val)

        engine.close()
        await engine.wait_closed()

        loop = asyncio.get_event_loop()
        loop.run_until_complete(self.add(loop))