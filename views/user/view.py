from aiohttp import web
from route_table import routes


@routes.view('/user')
class UserView(web.View):

    async def get(self):
        data = {'some': 'data'}
        return web.json_response(data)

